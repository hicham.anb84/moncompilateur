#include <string>
#include <iostream>
#include <cstdlib>
#include <set>

using namespace std;

enum OPREL {equ, diff, infe, supe, inf, sup, unknown};
char current, lookedAhead;               
int NLookedAhead=0;
set<string> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(char c){
        return DeclaredVariables.find(string(1,c))!=DeclaredVariables.end();
}

void ReadChar(void){
        if(NLookedAhead>0){
                current=lookedAhead;  
                NLookedAhead--;
        }
        else
                while(cin.get(current) && (current==' '||current=='\t'||current=='\n'));
}

void LookAhead(void){
        while(cin.get(lookedAhead) && (lookedAhead==' '||lookedAhead=='\t'||lookedAhead=='\n'));
        NLookedAhead++;
}

void Error(string s){
        cerr << "Lu : '"<<current<<"' , mais ";
        cerr<< s << endl;
        exit(-1);
}

void AdditiveOperator(void){
        if(current=='+'||current=='-')
                ReadChar();
        else
                if(current=='|'){
                        ReadChar();
                        if(current!='|')
                                Error("l'opérateur de comparaison s'écrit '||'");
                        else
                                ReadChar();
                }
                else
                        Error("Opérateur additif attendu");      
}
                
void Letter(void){
        if((current<'a')||(current>'z'))
                Error("lettre attendue");          
        else{
                cout << "\tpush "<<current<<endl;
                ReadChar();
        }
}

void Number(void){
        unsigned long long number;
        if((current<'0')||(current>'9'))
                Error("chiffre attendu");           
        else
                number=current-'0';
        ReadChar();
        while(current>='0' && current <='9'){
                number*=10;
                number+=current-'0';
                ReadChar();
        }
        cout <<"\tpush $"<<number<<endl;
}

void Expression(void);           

void Factor(void){
        if(current=='('){
                ReadChar();
                Expression();
                if(current!=')')
                        Error("')' était attendu");          
                else
                        ReadChar();
        }
        else 
                if (current>='0' && current <='9')
                        Number();
                else
                                if(current>='a' && current <='z')
                                        Letter();
                                else
                                        Error("'(' ou chiffre ou lettre attendue");
}

void MultiplicativeOperator(void){
        if(current=='*'||current=='/'||current=='%')
                ReadChar();
        else
                if(current=='&'){
                        ReadChar();
                        if(current!='&')
                                Error("l'opérateur ET s'écrit '&&'");
                        else
                                ReadChar();
                }
                else
                        Error("Opérateur multiplicatif attendu");          // Additive operator expected
}

void Term(void){
        char mulop;
        Factor();
        while(current=='*'||current=='/'||current=='%'||current=='&'){
                mulop=current;         
                MultiplicativeOperator();
                Factor();
                cout << "\tpop %rbx"<<endl;     
                cout << "\tpop %rax"<<endl;     
                switch(mulop){
                        case '*':
                        case '&':
                                cout << "\tmulq %rbx"<<endl;    
                                cout << "\tpush %rax"<<endl;    
                                break;
                        case '/':
                                cout << "\tmovq $0, %rdx"<<endl;         
                                cout << "\tdiv %rbx"<<endl;                     
                                cout << "\tpush %rax"<<endl;            
                                break;
                        case '%':
                                cout << "\tmovq $0, %rdx"<<endl;         
                                cout << "\tdiv %rbx"<<endl;                    
                                cout << "\tpush %rdx"<<endl;            
                                break;
                        default:
                                Error("opérateur additif attendu");
                }
        }
}

void SimpleExpression(void){
        char adop;
        Term();
        while(current=='+'||current=='-'||current=='|'){
                adop=current;          
                AdditiveOperator();
                Term();
                cout << "\tpop %rbx"<<endl;    
                cout << "\tpop %rax"<<endl;    
                if(adop=='+')
                        cout << "\taddq %rbx, %rax"<<endl;    
                else
                        cout << "\tsubq %rbx, %rax"<<endl;   
                cout << "\tpush %rax"<<endl;                
        }

}

void DeclarationPart(void){
        if(current!='[')
                Error("caractère '[' attendu");
        cout << "\t.data"<<endl;
        cout << "\t.align 8"<<endl;
        
        ReadChar();
        if(current<'a'||current>'z')
                Error("Une lettre minuscule était attendue");
        cout << current << ":\t.quad 0"<<endl;
        DeclaredVariables.insert(string(1,current));
        ReadChar();
        while(current==','){
                ReadChar();
                if(current<'a'||current>'z')
                        Error("Une lettre minuscule était attendue");
                cout << current << ":\t.quad 0"<<endl;
                DeclaredVariables.insert(string(1,current));
                ReadChar();
        }
        if(current!=']')
                Error("Caractère ']' attendu");
        ReadChar();
}


OPREL RelationalOperator(void){
        if(current!='<'&&current!='>'&&current!='!'&&current!='=')
                return unknown;
        LookAhead();
        if(lookedAhead=='=')
                switch(current){
                        case '=':
                                ReadChar(); ReadChar(); 
                                return equ;
                        case '!': 
                                ReadChar(); ReadChar(); 
                                return diff;
                        case '<': 
                                ReadChar(); ReadChar(); 
                                return infe;
                        case '>': 
                                ReadChar(); ReadChar(); 
                                return supe;
                }
        switch(current){
                case '=':    
                                Error("utilisez '==' comme opérateur d'égalité");
                case '<':       ReadChar();
                                return inf;
                case '>':       ReadChar();
                                return sup;
                default:        Error("opérateur relationnel inconnu");
                
        }
        return unknown; 
}

void Expression(void){
        OPREL oprel;
        SimpleExpression();
        if(current=='='||current=='!'||current=='<'||current=='>'){
                oprel=RelationalOperator();
                SimpleExpression();
                cout << "\tpop %rax"<<endl;
                cout << "\tpop %rbx"<<endl;
                cout << "\tcmpq %rax, %rbx"<<endl;
                switch(oprel){
                        case equ:
                                cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
                                break;
                        case diff:
                                cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
                                break;
                        case supe:
                                cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
                                break;
                        case infe:
                                cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
                                break;
                        case inf:
                                cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
                                break;
                        case sup:
                                cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
                                break;
                        default:
                                Error("Opérateur de comparaison inconnu");
                }
                cout << "\tpush $0\t\t# False"<<endl;
                cout << "\tjmp Suite"<<TagNumber<<endl;
                cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;       
                cout << "Suite"<<TagNumber<<":"<<endl;
        }
}

void AssignementStatement(void){
        char letter;
        if(current<'a'||current>'z')
                Error("lettre minuscule attendue");
        letter=current;
        if(!IsDeclared(letter)){
                cerr << "Erreur : Variable '"<<letter<<"' non déclarée"<<endl;
                exit(-1);
        }
        ReadChar();
        if(current!='=')
                Error("caractère '=' attendu");
        ReadChar();
        Expression();
        cout << "\tpop "<<letter<<endl;
}


void Statement(void){
        AssignementStatement();
}

void StatementPart(void){
        cout << "\t.text\t\t# The following lines contain the program"<<endl;
        cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
        cout << "main:\t\t\t# The main function body :"<<endl;
        cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
        Statement();
        while(current==';'){
                ReadChar();
                Statement();
        }
        if(current!='.')
                Error("caractère '.' attendu");
        ReadChar();
}

void Program(void){
        if(current=='[')
                DeclarationPart();
        StatementPart();        
}

int main(void){
        cout << "\t\t\t#This code was produced by the CERI Compiler"<<endl;
        ReadChar();
        Program();
        cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
        cout << "\tret\t\t\t# Return from main function"<<endl;
        if(cin.get(current)){
                cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
                Error(".");
        }

}
